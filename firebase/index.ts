import firebase from "firebase/app";
import "firebase/firestore";

// tryy 測試用 後續替換為公司用帳號
const firebaseConfig = {
  apiKey: "AIzaSyCVF_6Ziz5ZVg6V_f4mqRoZwO_zDzlf51Y",
  authDomain: "qiankun-main-846de.firebaseapp.com",
  // The value of `databaseURL` depends on the location of the database
  databaseURL:
    "https://qiankun-main-846de-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "qiankun-main-846de",
  storageBucket: "qiankun-main-846de.appspot.com",
  messagingSenderId: "394442827042",
  appId: "1:394442827042:web:1928bf35d524efd3b1b17b",
};

firebase.initializeApp(firebaseConfig);

// Initialize Cloud Firestore and get a reference to the service
export const db = firebase.firestore();
