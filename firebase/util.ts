import { db } from "./index";

interface ItemList {
  [code: string]: string | ItemList;
}
interface Router {
  [code: string]: string | [Router];
}

const baseUrl = "micro";
const configUrl = "config";
// 微服務，子項目存取位置 "micro/config/apps"
const appsUrl = "apps";
// 微服務，子項目 router 存取位置 "micro/config/router"
const routerUrl = "router";
// 子項目，存取位置 "micro/list"
const listUrl = "list";

// 獲取微服務子項目
export const getMicroApps = async () => {
  let list: ItemList[] = [];
  await db
    .collection(baseUrl)
    .doc(configUrl)
    .collection(`${configUrl}_coll`)
    .doc(appsUrl)
    .collection(`${appsUrl}_coll`)
    .get()
    .then((querySnapshot) => {
      const hasData = querySnapshot.docs.length !== 0;
      if (!hasData) return;
      querySnapshot.forEach((doc) => {
        // console.log("ggg-2", doc.id, doc.data());
        list.push(doc.data());
      });
    });
  return list;
};

// 儲存微服務子項目 setMicroApps ...有需要再補

// 儲存微服務子項目列表
export const setMicroAppsList = (arr: ItemList[]) => {
  arr.forEach((i: ItemList, index: Number) => {
    db.collection(baseUrl)
      .doc(configUrl)
      .collection(`${configUrl}_coll`)
      .doc(appsUrl)
      .collection(`${appsUrl}_coll`)
      .doc(`${index}`)
      .set(i)
      .then(() => {
        // console.log("Document successfully written!");
      })
      .catch((error) => {
        console.error("Error writing document: ", error);
      });
  });
};

// 監聽微服務，子項目異動
export const watchMicroApps = (callBack: any) => {
  db.collection(baseUrl)
      .doc(configUrl)
      .collection(`${configUrl}_coll`)
      .doc(appsUrl)
      .collection(`${appsUrl}_coll`)
      .onSnapshot((snapshot) => {
        // console.log("zzzzz-25", snapshot, snapshot.size)
        snapshot.size && callBack()
    });
};

// 獲取微服務子項目 router
export const getMicroRouter = async () => {
  let list: ItemList[] = [];
  await db
    .collection(baseUrl)
    .doc(configUrl)
    .collection(`${configUrl}_coll`)
    .doc(routerUrl)
    .collection(`${routerUrl}_coll`)
    .get()
    .then((querySnapshot) => {
      const hasData = querySnapshot.docs.length !== 0;
      if (!hasData) return;
      querySnapshot.forEach((doc) => {
        // console.log("ggg-2", doc.id, doc.data());
        list.push(doc.data());
      });
    });
  return list;
};

// 儲存微服務子項目 router
export const setMicroRouter = async (obj: Router) => {
  const baseData = await getMicroRouter();
  const index = baseData.length;
  const isAdd = baseData.find((i: any) => i.name === obj.name) === undefined;
  // console.log("sss-3", obj, baseData, isAdd);
  if (isAdd) {
    db.collection(baseUrl)
      .doc(configUrl)
      .collection(`${configUrl}_coll`)
      .doc(routerUrl)
      .collection(`${routerUrl}_coll`)
      .doc(`${index}`)
      .set(obj)
      .then(() => {
        // console.log("Document successfully written!");
      })
      .catch((error) => {
        console.error("Error writing document: ", error);
      });
  }
};

// 儲存微服務子項目 router 列表
export const setMicroRouterList = (arr: ItemList[]) => {
  arr.forEach((i: ItemList, index: Number) => {
    db.collection(baseUrl)
      .doc(configUrl)
      .collection(`${configUrl}_coll`)
      .doc(routerUrl)
      .collection(`${routerUrl}_coll`)
      .doc(`${index}`)
      .set(i)
      .then(() => {
        // console.log("Document successfully written!");
      })
      .catch((error) => {
        console.error("Error writing document: ", error);
      });
  });
};

// 監聽微服務，子項目 router 異動
export const watchMicroRouter = (callBack: any) => {
  db.collection(baseUrl)
    .doc(configUrl)
    .collection(`${configUrl}_coll`)
    .doc(routerUrl)
    .collection(`${routerUrl}_coll`)
    .onSnapshot((snapshot) => {
      // console.log("xxxxxxeeerrrrrr-1", snapshot, snapshot.size)
      snapshot.size && callBack();
    });
};

// 獲取子項目
export const getSubprojectList = async () => {
  const obj: ItemList = {};
  await db
    .collection(baseUrl)
    .doc(listUrl)
    .collection(`${listUrl}_coll`)
    .get()
    .then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        // console.log("ggg-2", doc.id, doc.data());
        obj[doc.id] = doc.data();
      });
    });
  return obj;
};

// 儲存子項目
export const setSubprojectList = (list: ItemList) => {
  const keys = Object.keys(list);
  keys.forEach((i) => {
    const data: any = list[i];
    // console.log("sss-2", i, data);
    db.collection(baseUrl)
      .doc(listUrl)
      .collection(`${listUrl}_coll`)
      .doc(i)
      .set(data)
      .then(() => {
        // console.log("Document successfully written!");
      })
      .catch((error) => {
        console.error("Error writing document: ", error);
      });
  });
};
