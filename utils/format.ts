// 文字轉換 - 首字大寫
export const firstToUpperCase = (str: string) => {
  return str.substr(0, 1).toUpperCase() + str.substr(1);
};
