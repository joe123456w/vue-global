import { Base64 } from "js-base64";

// tryy 要抽哪些共用...待後續新增
export const fun1 = (value: string) => {
  console.log("我是測試", value);
  return `我是測試 回傳資料-${value}`;
};

//加密規則
export const encode = (value: string) => {
  return Base64.encode(encodeURIComponent(JSON.stringify(value)));
};

//解密規則
export const decode = (value: string | null) => {
  return value ? JSON.parse(decodeURIComponent(Base64.decode(value))) : null;
};

export const common = {
  fun1,
  encode,
  decode,
};
