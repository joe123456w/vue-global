import { setMicroRouter } from "../firebase/util";
import { firstToUpperCase } from "./format";

/**
 * 單層式選單
 * 藉由傳入 routes 項目，並以此項內的 children 做動態設定
 * 需提供下述資訊:
 * meta.name:對應 qiankun path name
 * children.meta.label:對應左側選單 label 名稱
 * children.name:渲染 key 用
 * children.path:轉導用
 * children.meta.icon:左側選單 icon 呈現
 */
export const setMenu = (routes: any, findName = "layout") => {
  const obj = routes.find((i: any) => i.name === findName);
  if (obj) {
    const { meta, children }: any = obj;
    const itemName = meta.name;
    for (let i = 0; i < children.length; i++) {
      setTimeout(() => {
        const { meta, name, path } = children[i];
        const { label, icon } = meta;
        const info = {
          label,
          name: `${firstToUpperCase(itemName)}${firstToUpperCase(name)}`,
          path: `/sub/${itemName}${path}`,
          icon,
        };
        // console.log("dddd", label, info);
        setMicroRouter(info);
      }, 1000 * i);
    }
  }
};

/**
 * 二階式選單
 * 藉由傳入 routes 項目，並以此項內的 children 做動態設定
 * 需提供下述資訊:
 * meta.label:對應左側選單父層 label 名稱
 * meta.name:對應 qiankun path name
 * meta.icon:對應左側選單父層 icon 呈現
 * children.meta.label:對應左側選單子層 label 名稱
 * children.name:渲染 key 用
 * children.path:轉導用
 */
export const setSubMenu = (routes: any, findName = "layout") => {
  const obj = routes.find((i: any) => i.name === findName);
  if (obj) {
    const { meta, children }: any = obj;
    const { label, name, icon } = meta;
    const itemName = name;
    const childList: any = [];
    children.forEach((i: any) => {
      const { meta, name, path } = i;
      const childInfo = i.children.reduce((a: any, b: any) => {
        const { meta, name, path } = b;
        a.push({ label: meta.label, name, path })
        return a
      }, [])
      // console.log("ffffffwww-31", childInfo)
      childList.push({
        label: meta.label,
        name,
        path: `/sub/${itemName}${path}`,
        childInfo: [...childInfo]
      });
    });
    const menu = {
      label,
      name: firstToUpperCase(itemName),
      path: "",
      icon,
      children: childList,
    };
    setMicroRouter(menu);
  }
};
