import axios from "axios";

axios.defaults.baseURL = "/api";

interface Config {
  method: string;
  url: string;
  params?: string;
}

const server = async ({ method, url, params }: Config) => {
  // console.log("eeeee", method, url, params);
  const data = await axios({ method, url, params })
    .then((res) => {
      // console.log("eeeee-1", res);
      return res.data;
    })
    .catch((error) => {
      if (error.response) {
        const { status, data } = error.response;
        // console.log("eeeee-2", error, status);
        switch (status) {
          case 401:
            // tryy token 失效處理流程...待處理
            // tryy 會有 403 需求？
            console.log("token 失效流程...待處理", status);
            break;
          case 500:
            // tryy 500 錯誤流程...待處理
            console.log("500...待處理", status);
            break;
        }
        return data;
      } else {
        // tryy 斷網流程...待處理/定義
        console.log("斷網流程...待處理");
      }
    });
  return data;
};

axios.interceptors.request.use(
  (config) => {
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default server;
